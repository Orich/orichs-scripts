﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ScriptAPI;
using ScriptDotNet;


/*
This script is extremely sloppy, without a plethora of sanity checks, and was written between 3:00am and 3:41am
 
*/

namespace oEggs
{
    class Program
    {
        public static ushort[] SnakeTypes = new ushort[] { 0x0022, 0x0034 };

        static void ErrorExit(string ErrorText)
        {
            Console.WriteLine(ErrorText);
            Console.Write("Press any key to suicide.");
            Console.ReadKey(false);
            Environment.Exit(0);
        }

        static void Main(string[] args)
        {
            Find.FindDistance = 20;
            Find.FindVerticalDistance = 10;

            Console.WriteLine("Waiting for connection ...");
            while (!Profile.IsConnected) ;
            Console.WriteLine("Connection detected.  Welcome {0}!", Self.Name);

            if (Find.FindItems(0x2807, Self.Backpack.ID).Count < 1)
                ErrorExit("Not enough snake charmer flutes.  Bye.");

            Console.WriteLine("Press any key to start farming eggs.");
            Console.ReadKey(false);

            Console.WriteLine("Starting egg farm ...");

            while (Profile.IsConnected)
            {
                bool FluteSuccess = false;
                DateTime thisTime = DateTime.Now;

                // This is sloppy and doesnt check for no items found ... because at 3:00am i dont care
                List<Item> Snakes = Find.FindItems(SnakeTypes, new uint[] { 0x000 }, new ushort[] { 0xFFFF }).OrderBy(s => s.Distance).ToList();
                List<Item> Nests = Find.FindItems(0x2233).OrderBy(a => a.Distance).ToList();

                if (Nests.Count == 0)
                {
                    Console.WriteLine("No nests found, go to where nests are, then press a key");
                    Console.ReadKey(false);
                }

                List<Item> Flutes = Find.FindItems(0x2807, Self.Backpack.ID);
                if (Flutes.Count < 1)
                    ErrorExit("Ran out of flutes.");

                Item Flute = Flutes[0];

                if (Snakes.Count == 0)
                {
                    Console.WriteLine("I can't find any snakes.  Go to where snakes and nests are.");
                    Console.WriteLine("Press a key when ready");
                    Console.ReadKey(false);
                    continue;
                }

                for (int i = 0; i < Snakes.Count; i++)
                {
                    Console.WriteLine("Attempting flute process...");
                    while(true)
                    {
                        thisTime = DateTime.Now;
                        Flute.Use();
                        if (!Stealth.Script_WaitJournalLine(thisTime, "You must wait a moment for it to recharge.", 750))
                            break;

                        Console.WriteLine("Flute waiting to recharge.  Trying again in 1.5s.");
                        Script.Wait(1500);
                    }

                    Target.WaitForTarget();
                    Target.TargetObject(Snakes[i]);
                    Target.WaitForTarget();
                    thisTime = DateTime.Now;
                    Target.TargetObject(Nests[0]);
                    if (Stealth.Script_WaitJournalLine(thisTime, "The animal walks where it was instructed to.", 4000))
                    {
                        Console.WriteLine("Flute succeeded.");
                        FluteSuccess = true;
                        break;
                    }
                    Script.Wait(2000);
                }

                if (!FluteSuccess)
                {
                    Console.WriteLine("Flute failed.");
                    continue;
                }
                Console.WriteLine("Wait for snake to search ... ({0} seconds)", 15 - (DateTime.Now-thisTime).Seconds);
                if (!Stealth.Script_WaitJournalLine(thisTime, "The snake begins searching for rare eggs.", 15000))
                {
                    Console.WriteLine("Couldn't see if snake began searching.");
                    continue;
                }

                thisTime = DateTime.Now;

                bool SnakeFailed = true;
                while (DateTime.Now < thisTime.AddMilliseconds(10000))
                {
                    if (Stealth.Script_InJournalBetweenTimes("The snake finds a rare egg and drags it out of the nest!", thisTime, DateTime.Now) != -1)
                    {
                        SnakeFailed = false;
                        break;
                    }
                    if (
                    (Stealth.Script_InJournalBetweenTimes("Beware! The snake has hatched some of the eggs!!", thisTime, DateTime.Now) != -1) ||
                    (Stealth.Script_InJournalBetweenTimes("The snake searches the nest and finds nothing.", thisTime, DateTime.Now) != -1) ||
                    (Stealth.Script_InJournalBetweenTimes("The nest collapses.", thisTime, DateTime.Now) != -1)
                        )
                    {
                        break;
                    }
                }

                if (SnakeFailed)
                {
                    Console.WriteLine("Snaked failed.");
                    continue;
                }

                Console.WriteLine("Snake succeeded.");
                Console.WriteLine("Now I'll make sure you are hiding, then we'll try to stealth walk you to the egg");

                while (!Self.IsHidden)
                {
                    Self.UseSkill("Hiding");
                    Script.Wait(1500);
                }

                Console.WriteLine("Trying to activate Stealth (the skill!)");
                while (true)
                {
                    Script.Wait(2000);
                    thisTime = DateTime.Now;
                    Self.UseSkill("Stealth");
                    if (Stealth.Script_WaitJournalLine(thisTime, "You begin to move quietly", 3500))
                        break;
                    Script.Wait(3000);
                    Console.WriteLine("Stealth failed.  Trying again shortly.");
                }

                Stealth.Script_MoveXY((ushort)Nests[0].X, (ushort)Nests[0].Y, false, 0, false);

                Item Egg = Find.FindItem(0x41BF);
                if (Egg == null)
                {
                    Console.WriteLine("... Odd, I can't find the egg.  WTF?");
                    continue;
                }

                Console.WriteLine("Taking the egg.");
                Stealth.Script_DragItem(Egg.ID, 1);
                Script.Wait(1500);
                Stealth.Script_DropHere(Self.Backpack.ID);

                Script.Wait(3000);
            }


        }
    }
}
