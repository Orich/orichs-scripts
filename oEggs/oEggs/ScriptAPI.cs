﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace ScriptAPI
{
    using ScriptDotNet;

    #region Profile Methods
    public static class Profile
    {
        public static bool IsConnected
        {
            private set { }
            get
            {
                return Stealth.Script_GetConnectedStatus();
            }
        }
    }
    #endregion

    #region Script Methods
    public static class Script
    {
        public static void Wait(int WaitMS)
        {
            Stealth.Script_Wait(WaitMS);
        }
    }
    #endregion

    #region Item Find/Search Methods
    public static class Find
    {
        /// <summary>
        /// Find an Item
        /// </summary>
        /// <param name="Type">Item Type</param>
        /// <param name="Container">[Optional] ID of Container To Search [Default: 0x0 = Ground]</param>
        /// <param name="Recursive">[Optional] Search Sub-Containers Recursively [Default: False]</param>
        /// <param name="Color">[Optional] Color Category To Search[Default: 0xFFFF = All Colors]</param>
        /// <returns>Returns Item if Found or NULL if Not Found</returns>
        public static Item FindItem(ushort Type, uint Container = 0x00000000, bool Recursive = false, ushort Color = 0xFFFF)
        {
            uint finditem = Stealth.Script_FindTypeEx(Type, Color, Container, Recursive);
            if (finditem > 0)
                return new Item(finditem);
            else
                return null;
        }

        /// <summary>
        /// Find all items of a certain type
        /// </summary>
        /// <param name="Type">Item Type</param>
        /// <param name="Container">[Optional] ID of Container To Search [Default: 0x0 = Ground]</param>
        /// <param name="Recursive">[Optional] Search Sub-Containers Recursively [Default: False]</param>
        /// <param name="Color">[Optional] Color Category To Search[Default: 0xFFFF = All Colors]</param>
        /// <returns>List of Items Found</Item></returns>
        public static List<Item> FindItems(ushort Type, uint Container = 0x00000000, bool Recursive = false, ushort Color = 0xFFFF)
        {
            List<Item> AllList = new List<Item>();
            Stealth.Script_FindTypeEx(Type, Color, Container, Recursive);
            if (Stealth.Script_GetFindCount() == 0)
                return AllList;
            uint[] findlist = Stealth.Script_GetFindList();

            foreach (uint item in findlist)
                AllList.Add(new Item(item));

            return AllList;
        }

        /// <summary>
        /// Searches for items of Types[], in Containers[], and of Colors[]
        /// </summary>
        /// <param name="Types">Array of Item Types to search for</param>
        /// <param name="Containers">Array of Containers to search in [new uint[] = {0x00000000};] for Ground</param>
        /// <param name="Colors">Array of Colors to search for [new ushort[] = { 0xFFFF };] for all colors</param>
        /// <param name="Recursive">[Optional] Search Sub-Containers Recursively [Default: False]</param>
        /// <returns>List of Items Found</returns>
        public static List<Item> FindItems(ushort[] Types, uint[] Containers, ushort[] Colors, bool Recursive = false)
        {
            Stealth.Script_FindTypesArrayEx(Types, Colors, Containers, Recursive);
            uint[] findlist = Stealth.Script_GetFindList();
            List<Item> AllList = new List<Item>();
            foreach (uint item in findlist)
                AllList.Add(new Item(item));
            return AllList;
        }

        /// <summary>
        /// Set distance (from player) to scan for items
        /// </summary>
        public static uint FindDistance
        {
            get
            {
                return Stealth.Script_GetFindDistance();
            }
            set
            {
                Stealth.Script_SetFindDistance(value);
            }
        }

        /// <summary>
        /// Set Vertical distance (from player) to scan for items
        /// </summary>
        public static uint FindVerticalDistance
        {
            get
            {
                return Stealth.Script_GetFindVertical();
            }
            set
            {
                Stealth.Script_SetFindVertical(value);
            }
        }
    }
    #endregion

    #region Target / Targeting Methods
    public static class Target
    {
        public static Item RequestTarget(uint TimeoutMS = 0)
        {
            Stealth.Script_ClientRequestObjectTarget();
            Stopwatch timer = new Stopwatch();


            timer.Start();
            while (Stealth.Script_ClientTargetResponsePresent() == false)
            {
                if (TimeoutMS != 0 && timer.ElapsedMilliseconds >= TimeoutMS)
                    return default(Item);
            }

            return new Item(Stealth.Script_ClientTargetResponse().ID);
        }

        public static void WaitForTarget(int TimeoutMS = 10000)
        {
            Stealth.Script_WaitForTarget(TimeoutMS);
        }

        public static void TargetObject(Item obj)
        {
            Stealth.Script_TargetToObject(obj.ID);
        }
    }
    #endregion

    // These aren't very important to most people
    #region BaseType
    /// <summary>
    /// Where it all starts.  Every thing has an Item Type.
    /// </summary>
    public class BaseType
    {
        private readonly ushort _type;
        public ushort Type { private set { } get { return _type; } }

        protected BaseType(uint ID)
        {
            _type = Stealth.Script_GetType(ID);
        }
        public BaseType(ushort Type)
        {
            _type = Type;
        }
    }
    #endregion
    #region BaseThing
    /// <summary>
    /// Most basic class to describe any "thing" in UO
    /// ID, Type, X, Y, Z
    /// </summary>
    public class BaseThing : BaseType, IEquatable<BaseThing>
    {
        protected uint _id;

        public BaseThing(uint ID)
            : base(ID)
        {
            _id = ID;
        }
        public BaseThing(BaseThing thing)
            : base(thing.ID)
        {
            _id = thing.ID;
        }
        public BaseThing(uint ID, ushort TYPE)
            : base(TYPE)
        {
            _id = ID;
        }

        /// <summary>
        /// X Coordinate [Relative to container]
        /// </summary>
        public int X
        {
            private set { }
            get
            {
                return Stealth.Script_GetX(_id);
            }
        }
        /// <summary>
        /// Y Coordinate [Relative to container]
        /// </summary>
        public int Y
        {
            private set { }
            get
            {
                return Stealth.Script_GetY(_id);
            }
        }
        /// <summary>
        /// Z Coordinate [Relative to container]
        /// </summary>
        public sbyte Z
        {
            private set { }
            get
            {
                return Stealth.Script_GetZ(_id);
            }
        }

        /// <summary>
        /// UO Unique Identifier Number
        /// </summary>
        public uint ID { private set { } get { return _id; } }

        public override int GetHashCode()
        {
            return this._id.GetHashCode();
        }

        public override bool Equals(Object obj)
        {
            BaseThing other = obj as BaseThing;
            if (other == null)
                return false;
            return (this.ID.Equals(other.ID));
        }

        public bool Equals(BaseThing other)
        {
            if (other == null)
                return false;
            return (this._id.Equals(other.ID));
        }

        public static bool operator ==(BaseThing a, BaseThing b)
        {
            if (object.ReferenceEquals(a, b)) return true;
            if (object.ReferenceEquals(a, null)) return false;
            if (object.ReferenceEquals(b, null)) return false;

            return a.Equals(b);
        }
        public static bool operator !=(BaseThing a, BaseThing b)
        {
            if (object.ReferenceEquals(a, b)) return !true;
            if (object.ReferenceEquals(a, null)) return !false;
            if (object.ReferenceEquals(b, null)) return !false;

            return !a.Equals(b);
        }
    }
    #endregion

    // These are the important ones
    #region Item
    /// <summary>
    /// Basic Item class (all in-game objects derive from this)
    /// </summary>
    public class Item : BaseThing
    {
        public void Click()
        {
            Stealth.Script_ClickOnObject(_id);
        }

        public void Use()
        {
            Stealth.Script_UseObject(_id);
        }

        /// <summary>
        /// True/False if Item Exists
        /// </summary>
        public bool IsExists
        {
            private set { }
            get
            {
                return Stealth.Script_IsObjectExists(_id);
            }
        }
        public bool IsNPC
        {
            private set { }

            get
            {
                return Stealth.Script_IsNPC(_id);
            }
        }
        public bool IsContainer
        {
            private set { }
            get
            {
                return Stealth.Script_IsContainer(_id);
            }
        }
        public bool IsMovable
        {
            private set { }
            get
            {
                return Stealth.Script_IsMovable(_id);
            }
        }



        /// <summary>
        /// Returns distance from player
        /// [-1 if invalid / non-existant]
        /// </summary>
        public int Distance
        {
            private set { }
            get
            {
                return Stealth.Script_GetDistance(_id);
            }
        }

        /// <summary>
        /// Tooltip describing properties of the item
        /// [Value = NULL if unable to Scan]
        /// </summary>
        public string Tooltip
        {
            private set { }
            get
            {
                return Stealth.Script_GetTooltip(_id);
            }
        }

        /// <summary>
        /// UO Color Value
        /// </summary>
        public ushort Color
        {
            private set { }
            get
            {
                return Stealth.Script_GetColor(_id);
            }
        }

        /// <summary>
        /// Quantity of this ID
        /// </summary>
        public int Quantity
        {
            private set { }
            get
            {
                return Stealth.Script_GetQuantity(_id);
            }
        }

        /// <summary>
        /// UO Name Value
        /// </summary>
        public string Name
        {
            private set { }
            get
            {
                return Stealth.Script_GetName(_id);
            }
        }

        public Item(uint ID)
            : base(ID)
        {
        }
        public Item(BaseThing thing)
            : base(thing.ID)
        {
        }
    }
    #endregion

    #region Container
    public class Container : Item
    {
        public Container(uint ID)
            : base(ID)
        {
        }
        public Container(BaseThing thing)
            : base(thing.ID)
        {
        }


        public List<Item> Inventory(ushort FindType = 0xFFFF, bool Recursive = true)
        {
            return Find.FindItems(FindType, _id, Recursive);
        }

        public bool Open()
        {
            base.Use();
            Stealth.Script_Wait(250);
            if (Stealth.Script_GetLastContainer() == _id)
                return true;

            return false;
        }

        public void Close()
        {
            Stealth.Script_CloseClientUIWindow(TUIWindowType.wtContainer, _id);
        }
    }
    #endregion

    #region Creature
    /// <summary>
    /// Creature Class
    /// Base type for all living things
    /// Properties Include MaxHP, HP (current), Notoriety
    /// </summary>
    public class Creature : Item
    {
        /// <summary>
        /// Maximum HP of Creature
        /// </summary>
        public int MaxHP
        {
            private set { }
            get
            {
                return Stealth.Script_GetMaxHP(ID);
            }
        }

        /// <summary>
        /// Current HP of Creature
        /// </summary>
        public int HP
        {
            private set { }
            get
            {
                return Stealth.Script_GetHP(ID);
            }
        }

        /// <summary>
        /// Notoriety of Creature
        /// 1: Innocent (Blue)
        /// 2: Guild / Alliance (Green)
        /// 3: Attackable But Not Criminal (Gray)
        /// 4: Criminal (gray)
        /// 5: Enemy (orange)
        /// 6: Murderer (red)
        /// </summary>
        public byte Notoriety
        {
            private set { }
            get
            {
                return Stealth.Script_GetNotoriety(ID);
            }
        }

        public Creature(uint ID)
            : base(ID)
        {
        }
        public Creature(BaseThing thing)
            : base(thing.ID)
        {
        }

    }
    #endregion

    #region NPC
    public class NPC : Creature
    {
        public uint RightHand
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetRhandLayer(), ID);
            }
        }

        public uint LeftHand
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetLhandLayer(), ID);
            }
        }

        public uint Shoes
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetShoesLayer(), ID);
            }
        }

        public uint Pants
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetPantsLayer(), ID);
            }
        }

        public uint Shirt
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetShirtLayer(), ID);
            }
        }

        public uint Hat
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetHatLayer(), ID);
            }
        }

        public uint Gloves
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetGlovesLayer(), ID);
            }
        }

        public uint Ring
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetRingLayer(), ID);
            }
        }

        public uint Talisman
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetTalismanLayer(), ID);
            }
        }

        public uint Neck
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetNeckLayer(), ID);
            }
        }

        public uint Hair
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetHairLayer(), ID);
            }
        }

        public uint Waist
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetWaistLayer(), ID);
            }
        }

        public uint Torso
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetTorsoLayer(), ID);
            }
        }

        public uint Brace
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetBraceLayer(), ID);
            }
        }

        public uint Beard
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetBeardLayer(), ID);
            }
        }

        public uint TorsoH
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetTorsoHLayer(), ID);
            }
        }

        public uint Ear
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetEarLayer(), ID);
            }
        }

        public uint Arms
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetArmsLayer(), ID);
            }
        }

        public uint Cloak
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetCloakLayer(), ID);
            }
        }

        public Container Backpack
        {
            private set { }
            get
            {
                return new Container(Stealth.Script_ObjAtLayerEx(Stealth.Script_GetBpackLayer(), ID));
            }
        }

        public uint Robe
        {
            private set { }
            get
            {
                return Stealth.Script_ObjAtLayerEx(Stealth.Script_GetRobeLayer(), ID);
            }
        }

        public List<Item> Inventory
        {
            private set { }
            get
            {
                return this.Backpack.Inventory();
            }
        }

        public NPC(uint ID)
            : base(ID)
        {
        }
        public NPC(BaseThing thing)
            : base(thing.ID)
        {
        }

    }
    #endregion

    #region Self
    public static class Self
    {
        private static TExtendedInfo extInfo;
        public static uint ID { private set { } get { return Stealth.Script_GetSelfID(); } }

        public static int Mana { private set { } get { return Stealth.Script_GetSelfMana(); } }
        public static int HP { private set { } get { return Stealth.Script_GetSelfLife(); } }
        public static int Life { private set { } get { return HP; } }

        public static int X { private set { } get { return Stealth.Script_GetX(ID); } }
        public static int Y { private set { } get { return Stealth.Script_GetY(ID); } }

        public static string Name
        {
            private set { }
            get
            {
                return Stealth.Script_GetCharName();
            }
        }

        public static bool IsHidden
        {
            private set { }
            get
            {
                return Stealth.Script_GetHiddenStatus();
            }
        }

        public static Container Backpack
        {
            private set { }
            get
            {
                return new Container(Stealth.Script_GetBackpackID());
            }
        }

        public static byte World
        {
            private set { }
            get
            {
                return Stealth.Script_GetWorldNum();
            }
        }

        public static string Shard
        {
            private set { }
            get
            {
                return Stealth.Script_GetShardName();
            }
        }

        public static uint LastContainer
        {
            private set { }
            get
            {
                return Stealth.Script_GetLastContainer();
            }
        }

        public static void Cast(string SpellName, Item Object = null)
        {
            if (Object != null)
                Stealth.Script_CastSpellToObj(SpellName, Object.ID);
            else
                Stealth.Script_CastSpell(SpellName);
        }

        public static bool UseSkill(string Skill)
        {
            return Stealth.Script_UseSkill(Skill);
        }

        public static void UsePrimary()
        {
            Stealth.Script_UsePrimaryAbility();
        }
        public static void UseSecondary()
        {
            Stealth.Script_UseSecondaryAbility();
        }
        public static string ActiveAbility
        {
            private set { }
            get
            {
                return Stealth.Script_GetAbility();
            }
        }

        public static bool WarMode
        {
            set
            {
                Stealth.Script_SetWarMode(value);
            }
            get
            {
                return Stealth.Script_GetWarModeStatus();
            }
        }

        public static Creature Target
        {
            set
            {
                Stealth.Script_Attack(value.ID);
            }
            get
            {
                uint ret = Stealth.Script_GetWarTarget();
                if (ret == 0)
                    return null;
                return new Creature(Stealth.Script_GetWarTarget());
            }
        }

        static Self()
        {
            Properties.Refresh();
        }

        public static class Properties
        {
            /// <summary>
            /// Get updated properties
            /// OSI Shards Only
            /// </summary>
            public static void Refresh()
            {
                extInfo = Stealth.Script_GetExtInfo();
            }

            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort DamageMin { get { return extInfo.DamageMin; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort DamageMax { get { return extInfo.DamageMax; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static uint Tithing_Points { get { return extInfo.Tithing_Points; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Hit_Chance_Incr { get { return extInfo.Hit_Chance_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Swing_Speed_Incr { get { return extInfo.Swing_Speed_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Damage_Chance_Incr { get { return extInfo.Damage_Chance_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Lower_Reagent_Cost { get { return extInfo.Lower_Reagent_Cost; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort HP_Regen { get { return extInfo.HP_Regen; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Stam_Regen { get { return extInfo.Stam_Regen; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Mana_Regen { get { return extInfo.Mana_Regen; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Reflect_Phys_Damage { get { return extInfo.Reflect_Phys_Damage; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Enhance_Potions { get { return extInfo.Enhance_Potions; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Defense_Chance_Incr { get { return extInfo.Defense_Chance_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Spell_Damage_Incr { get { return extInfo.Spell_Damage_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Faster_Cast_Recovery { get { return extInfo.Faster_Cast_Recovery; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Faster_Casting { get { return extInfo.Faster_Casting; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Lower_Mana_Cost { get { return extInfo.Lower_Mana_Cost; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Strength_Incr { get { return extInfo.Strength_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Dext_Incr { get { return extInfo.Dext_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Int_Incr { get { return extInfo.Int_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort HP_Incr { get { return extInfo.HP_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Stam_Incr { get { return extInfo.Stam_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Mana_Incr { get { return extInfo.Mana_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Max_HP_Incr { get { return extInfo.Max_HP_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Max_Stam_Incr { get { return extInfo.Max_Stam_Incr; } private set { } }
            /// <summary>
            /// OSI Shards Only
            /// </summary>
            public static ushort Max_Mana_Increase { get { return extInfo.Max_Mana_Increase; } private set { } }

            public static short Luck
            {
                get
                {
                    short luck = Convert.ToInt16(Stealth.Script_GetSelfLuck());
                    if (luck != 0)
                        return luck;
                    return extInfo.Luck;
                }
                private set { }
            }

            public static Byte Sex { private set { } get { return Stealth.Script_GetSelfSex(); } }
            public static String Title { private set { } get { return Stealth.Script_GetCharTitle(); } }
            public static uint Gold { private set { } get { return Stealth.Script_GetSelfGold(); } }
            public static ushort Weight { private set { } get { return Stealth.Script_GetSelfWeight(); } }
            public static ushort MaxWeight { private set { } get { return Stealth.Script_GetSelfMaxWeight(); } }
            public static Byte Race { private set { } get { return Stealth.Script_GetSelfRace(); } }
            public static Byte MaxPets { private set { } get { return Stealth.Script_GetSelfPetsMax(); } }
            public static Byte Pets { private set { } get { return Stealth.Script_GetSelfPetsCurrent(); } }

            public static ushort PhysicalResist { private set { } get { return Stealth.Script_GetSelfArmor(); } }
            public static ushort FireResist { private set { } get { return Stealth.Script_GetSelfFireResist(); } }
            public static ushort ColdResist { private set { } get { return Stealth.Script_GetSelfColdResist(); } }
            public static ushort PoisonResist { private set { } get { return Stealth.Script_GetSelfPoisonResist(); } }
            public static ushort EnergyResist { private set { } get { return Stealth.Script_GetSelfEnergyResist(); } }
        }
    }
    #endregion

    #region GumpClass
    public class GumpClass
    {
        private uint _gumpid, _gumptype;
        private int _gumpidx;
        private TGumpInfo _gumpinfo;

        private GumpClass(uint Serial, uint GumpType)
        {
            _gumpid = Serial;
            _gumptype = GumpType;
            _gumpidx = GetIndexbySerial(Serial);
            _gumpinfo = Stealth.Script_GetGumpInfo(Convert.ToUInt16(_gumpidx));
        }

        public uint ID
        {
            private set { }
            get
            {
                return _gumpid;
            }
        }

        public bool IsExists()
        {
            for (ushort i = 0; i < GumpCount; i++)
            {
                if (Stealth.Script_GetGumpSerial(i) == _gumpid)
                    return true;
            }
            return false;
        }

        public string[] Textlines()
        {
            return Stealth.Script_GetGumpTextLines((ushort)_gumpidx);
        }

        public List<TButtonTileArt> ButtonTileArts { get { return _gumpinfo.ButtonTileArts.ToList(); } private set { } }
        public List<TCheckBox> CheckBoxes { get { return _gumpinfo.CheckBoxes.ToList(); } private set { } }
        public List<TCheckerTrans> CheckerTrans { get { return _gumpinfo.CheckerTrans.ToList(); } private set { } }
        public List<TCroppedText> CroppedText { get { return _gumpinfo.CroppedText.ToList(); } private set { } }
        public List<TEndGroup> EndGroups { get { return _gumpinfo.EndGroups.ToList(); } private set { } }
        public List<TGroup> Groups { get { return _gumpinfo.Groups.ToList(); } private set { } }
        public List<TGumpButton> GumpButtons { get { return _gumpinfo.GumpButtons.ToList(); } private set { } }
        public List<TGumpPic> GumpPics { get { return _gumpinfo.GumpPics.ToList(); } private set { } }
        public List<TGumpPicTiled> GumpPicTiled { get { return _gumpinfo.GumpPicTiled.ToList(); } private set { } }
        public List<TGumpText> GumpText { get { return _gumpinfo.GumpText.ToList(); } private set { } }
        public List<THtmlGump> HtmlGump { get { return _gumpinfo.HtmlGump.ToList(); } private set { } }
        public List<TItemProperty> ItemProperties { get { return _gumpinfo.ItemProperties.ToList(); } private set { } }
        public List<TRadio> RadioButtons { get { return _gumpinfo.RadioButtons.ToList(); } private set { } }
        public List<TResizePic> ResizePics { get { return _gumpinfo.ResizePics.ToList(); } private set { } }
        public List<TTextEntry> TextEntries { get { return _gumpinfo.TextEntries.ToList(); } private set { } }
        public List<TTextEntryLimited> TextEntriesLimited { get { return _gumpinfo.TextEntriesLimited.ToList(); } private set { } }
        public List<TTilePichue> TilePicHue { get { return _gumpinfo.TilePicHue.ToList(); } private set { } }
        public List<TTilePic> TilePics { get { return _gumpinfo.TilePics.ToList(); } private set { } }
        public List<TTooltip> Tooltips { get { return _gumpinfo.Tooltips.ToList(); } private set { } }
        public List<TXmfHTMLGump> XmfHtmlGump { get { return _gumpinfo.XmfHtmlGump.ToList(); } private set { } }
        public List<TXmfHTMLGumpColor> XmfHTMLGumpColor { get { return _gumpinfo.XmfHTMLGumpColor.ToList(); } private set { } }
        public List<TXmfHTMLTok> XmfHTMLTok { get { return _gumpinfo.XmfHTMLTok.ToList(); } private set { } }

        #region Click Events
        public bool ClickButton(int ButtonID)
        {
            if (IsExists())
                return Stealth.Script_NumGumpButton(Convert.ToUInt16(_gumpidx), ButtonID);

            return false;
        }

        public bool ToggleRadioButton(int RadioID, int Value)
        {
            if (IsExists())
                return Stealth.Script_NumGumpRadiobutton(Convert.ToUInt16(_gumpidx), RadioID, Value);

            return false;
        }

        public bool ToggleCheckBoxButton(int CheckBoxID, int Value)
        {
            if (IsExists())
                return Stealth.Script_NumGumpCheckBox(Convert.ToUInt16(_gumpidx), CheckBoxID, Value);

            return false;
        }

        public bool ChangeTextField(int TextFieldID, String Text)
        {
            if (IsExists())
                return Stealth.Script_NumGumpTextEntry(Convert.ToUInt16(_gumpidx), TextFieldID, Text);

            return false;
        }
        #endregion

        #region Static Members
        public static GumpClass Create(ushort index)
        {
            uint Serial = Stealth.Script_GetGumpSerial(index);
            ushort Type = Convert.ToUInt16(Stealth.Script_GetGumpID(index));

            return new GumpClass(Serial, Type);
        }

        public static GumpClass LastGump()
        {
            int index = GumpCount - 1;

            if (index >= 0)
            {
                return GumpClass.Create((ushort)index);
            }

            return default(GumpClass);
        }

        public static uint GetGumpType(uint Serial)
        {
            int index = GetIndexbySerial(Serial);
            if (index > -1)
                return Stealth.Script_GetGumpID(Convert.ToUInt16(index));
            return 0;
        }

        public static uint GetSerial(uint GumpType)
        {
            int index = GetIndexbyType(GumpType);
            if(index>-1)
                return Stealth.Script_GetGumpSerial(Convert.ToUInt16(index));
            return 0;
        }

        public static int GetIndexbyType(uint GumpType)
        {
            for (ushort i = 0; i < GumpCount; i++)
            {
                if (Stealth.Script_GetGumpID(i) == GumpType)
                    return i;
            }
            return -1;
        }

        public static int GetIndexbySerial(uint GumpSerial)
        {
            for (ushort i = 0; i < GumpCount; i++)
            {
                if (Stealth.Script_GetGumpSerial(i) == GumpSerial)
                    return i;
            }
            return -1;
        }

        public static ushort GumpCount
        {
            private set { }
            get
            {
                return Stealth.Script_GetGumpsCount();
            }
        }
        #endregion
    }
    #endregion

    #region Context Menu
    public class ContextMenu
    {
        Creature _owner;

        public ContextMenu(uint Owner)
        {
            _owner = new Creature(Owner);
        }

        public bool Click(uint ClilocID)
        {
            if ((_owner.IsExists) && (_owner.Distance <= 3))
            {
                Stealth.Script_ClearContextMenu();
                Stealth.Script_RequestContextMenu(_owner.ID);
                String Text = "";
                if ((Text = Stealth.Script_GetContextMenu()).Trim() != "")
                {
                    Text = Text.Replace("\r\n", "¶");
                    String[] E = Text.Split(new char[] { '¶' });
                    foreach (String a in E)
                    {
                        String[] b = a.Split(new char[] { '|' });
                        if (Convert.ToUInt32(b[1]) == ClilocID)
                        {
                            Stealth.Script_SetContextMenuHook(_owner.ID, Convert.ToByte(b));
                            Stealth.Script_RequestContextMenu(_owner.ID);
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
    #endregion
}
